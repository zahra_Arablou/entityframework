﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day10TodosEF
{
   public class Todo
    {

        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        private string _task;
        public string Task {
            get
            {
                return _task;
            } 
            set 
            {
                Regex regex = new Regex("^[^;]{1,100}$");
                if (!regex.IsMatch(value))
                {
                    throw new InvalidValueException("Task shoud be 1 to 100 character except ;");
                }
                _task = value;
            }
        }
        [Required]
        public int Difficulty { get; set; }
        [Required]
        public DateTime DueDate { get; set; }
        [Required]

        [EnumDataType(typeof(StatusEnum))]
        public StatusEnum Status { get; set; }
        public enum StatusEnum {Pending=1,Done=2,Delegated=3 }

        public Todo(int id, string task, int difficulty, DateTime dueDate, StatusEnum status)
        {
            Id = id;
            Task = task;
            Difficulty = difficulty;
            DueDate = dueDate;
            Status = status;
        }

        public Todo() :base()
        {

        }
    }
}
