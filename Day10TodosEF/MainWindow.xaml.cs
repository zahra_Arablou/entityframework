﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10TodosEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TasksDbContext context;
        List<Todo> listTodo = new List<Todo>();
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                context = new TasksDbContext();
                comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatusEnum));
                comboStatus.SelectedIndex =0;
                loadRecordsfromDb();
            }
            catch(SystemException ex)
            {
                MessageBox.Show("fatal Error connecting to database." + ex.Message);
                Environment.Exit(1);
            }catch(InvalidValueException ex)
            {
                MessageBox.Show(ex.Message);

            }


        }
       void loadRecordsfromDb()
        {
            listTodo = (from t in context.Todos select t).ToList<Todo>();
            lvTodo.ItemsSource = listTodo;
        }
        void clearFields()
        {
            lblId.Content = "...";
            tbTask.Clear();
            sliderDiff.Value = 3;
            datepicker.SelectedDate = null;
            comboStatus.SelectedIndex = 0;

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string task = tbTask.Text;
                int diff = (int)sliderDiff.Value;
                if(datepicker.SelectedDate==null)
                {
                    MessageBox.Show("select a date first.");
                    return;
                }
                DateTime dueDate = (DateTime)datepicker.SelectedDate;
                
                Todo.StatusEnum status = (Todo.StatusEnum)comboStatus.SelectedItem;
                Todo t1 = new Todo(0, task, diff, dueDate, status);

                context.Todos.Add(t1);
                context.SaveChanges();
                loadRecordsfromDb();
                clearFields();
            }catch(SystemException ex)
            {
                MessageBox.Show("Database Oprtion failed." + ex.Message);
            }catch(InvalidValueException ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btDel_Click(object sender, RoutedEventArgs e)
        {
            if(lvTodo.SelectedIndex<0)
            {
                MessageBox.Show("Select an item to delete.");
                return;
            }
            int tId = (int)lblId.Content;
            Todo t1 = (from t in context.Todos where t.Id == tId select t).FirstOrDefault<Todo>();
            if (t1 != null)
            {
                context.Todos.Remove(t1);
                context.SaveChanges();
                loadRecordsfromDb();
                clearFields();
                MessageBox.Show("Record Deleted", "Success", MessageBoxButton.OK, MessageBoxImage.Information);

            }

        }

        private void lvTodo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lvTodo.SelectedIndex<0)
            {
                return;
            }
           
            Todo todo = listTodo[lvTodo.SelectedIndex];
            lblId.Content = todo.Id;
            tbTask.Text = todo.Task;
            sliderDiff.Value = todo.Difficulty;
            comboStatus.SelectedItem = todo.Status;
            int tId = todo.Id;
       }
        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvTodo.SelectedIndex < 0)
            {
                MessageBox.Show("Select an item to Update.");
                return;
            }
            try
            {
                int tId = (int)lblId.Content;
                Todo t1 = (from t in context.Todos where t.Id == tId select t).FirstOrDefault<Todo>();
                if (t1 != null)
                {
                    t1.Task = tbTask.Text;
                    t1.Difficulty = (int)sliderDiff.Value;
                    t1.Status = (Todo.StatusEnum)comboStatus.SelectedItem;
                    t1.DueDate = (DateTime)datepicker.SelectedDate;
                    if (datepicker.SelectedDate == null)
                    {
                        MessageBox.Show("select a date first.");
                        return;
                    }
                    context.SaveChanges();
                    loadRecordsfromDb();
                    clearFields();
                    MessageBox.Show("Record Updated", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            } catch (SystemException ex)
            {
                MessageBox.Show("Database Oprtion failed." + ex.Message);
            }
            catch (InvalidValueException ex)
            {
                MessageBox.Show(ex.Message);

            }

        }
    }
}
