﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10TodosEF
{
    class TasksDbContext:DbContext
    {
        public TasksDbContext() : base("Day10TodoEFDb") { }
        virtual public DbSet<Todo> Todos { get; set; }

    }
}
