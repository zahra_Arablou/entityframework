﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class Person
    {   [Key]
        public int Id { get; set; }
        [Required]//not null
        [StringLength(50)]  //nvarchar(5)
        public string Name { get; set; }
        [Index]//not unique, speeds up lookup operations
        public int Age { get; set; }

        //[NotMapped]//in memory only,not in database
        //public string Comment { get; set; }

        //[EnumDataType(typepof(GenderEnum))]
        //public GenderEnum Gender { get; set; }
        //public GenderEnum {Male=1,Female=2,NA=3} 
        

    }
}
