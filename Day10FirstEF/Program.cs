﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SocietyDbContext context = new SocietyDbContext();
                //equivalent of insert
                //Person p1 = new Person() { Name = "Jerry", Age = 21 };
              // Person p2 = new Person() { Name = "Ali ", Age = 15 };
                //context.People.Add(p1);
               //context.People.Add(p2);
                
                context.SaveChanges();
                Console.WriteLine("record Added.");

                // equivalant of update-fetch then modify,save changes 

                Person p3 = (from p in context.People where p.Id == 1 select p).FirstOrDefault<Person>();
                if(p3!=null)
                {
                    //found the record to update
                    p3.Name="ALIIIIIIIIII" + 2333;//Entity Framework is watching and notices the modification

                    context.SaveChanges();
                    Console.WriteLine("record updated");
                }
                else
                {
                    Console.WriteLine("record to update not found");
                }
                //Delete - fetch then Schedual  for deletion ,  then save changes
                Person p4 = (from p in context.People where p.Id == 6 select p).FirstOrDefault<Person>();
                if(p4!=null)
                {
                    // found the record to delete
                    context.People.Remove(p4);
                    context.SaveChanges();
                    Console.WriteLine("record deleted");
                }
                else
                {
                    Console.WriteLine("record to delete not found");
                }
                //fetch all records
                var peopleCol = (from p in context.People select p);
                foreach (Person p in peopleCol)
                    Console.WriteLine( $"{p.Id} name :{p.Name} and {p.Age}y/o");
            }
            finally
            {
                Console.WriteLine("Press a key");
                Console.ReadKey();
            }
        }
    }
}
