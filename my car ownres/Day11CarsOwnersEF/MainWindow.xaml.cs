﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Owner> listOwner = new List<Owner>();
        CarsOwnersDbContext context;
        byte[] currOwnerImage;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                context = new CarsOwnersDbContext();
                RefreshLvOwners();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1); // fatal error
            }
        }
        void RefreshLvOwners()
        {
            try
            {
                lblId.Content = "";
                tbName.Clear();
                btManageCar.IsEnabled = false;
                btDelete.IsEnabled = false;
                btUpdate.IsEnabled = false;
                tbImage.Visibility = Visibility.Visible;
                imageViewer.Source = null;
                // lvOwner.ItemsSource = (from o in context.Owners select o).ToList<Owner>();
                // Include means to force eager loading - used for collections in OneToMany relations
                lvOwner.ItemsSource = context.Owners.Include("CarsInGarage").ToList<Owner>();
                Utils.AutoResizeColumns(lvOwner);//TODO

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        //TODO:  
        //private void ResizeGridViewColumn(GridViewColumn column)
        //{
        //    if (double.IsNaN(column.Width))
        //    {
        //        column.Width = column.ActualWidth;
        //    }
        //    column.Width = double.NaN;
        //}

        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currOwnerImage = File.ReadAllBytes(dlg.FileName);
                    tbImage.Visibility = Visibility.Hidden;
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currOwnerImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        public bool IsFieldsValid()
        {
            if (tbName.Text.Length < 2)
            {
                MessageBox.Show("Name must be between 2 and 100 characters", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (currOwnerImage == null)
            {
                MessageBox.Show("Choose a picture", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }


        private void btManageCar_Click(object sender, RoutedEventArgs e)
        {
            Owner owner = (Owner)lvOwner.SelectedItem;

           

            CarsDialog carsDialog = new CarsDialog(owner) { Owner=this};
            carsDialog.ShowDialog();
            RefreshLvOwners();
        }
      
        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            try
            {
                string name = tbName.Text;
                Owner o = new Owner() { Name = name, Photo = currOwnerImage };
                context.Owners.Add(o);
                context.SaveChanges();
                RefreshLvOwners();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            Owner o= (Owner)lvOwner.SelectedItem;
           
            if (o != null)
            {
                if (MessageBox.Show("Are you sure?", "Update", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
                {

                    o.Name = tbName.Text;
                    o.Photo = currOwnerImage;
                    context.SaveChanges();
                    RefreshLvOwners();
                }
            }

        }
        

        private void lvOwner_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvOwner.SelectedIndex == -1)
            {
                RefreshLvOwners();
                return;
            }

          
                Owner o = (Owner)lvOwner.SelectedItem;
                tbName.Text = o.Name;
                lblId.Content = o.Id;
            currOwnerImage = o.Photo;
            imageViewer.Source = Utils.GetBitmapImageV2(o.Photo);
            btDelete.IsEnabled = true;
            btUpdate.IsEnabled = true;
            btManageCar.IsEnabled = true;
        }
        

        private void btImage_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            Owner ownerCurr = (Owner)lvOwner.SelectedItem;
            if (ownerCurr == null) { return; }
            if (MessageBoxResult.Yes != MessageBox.Show("Do you want to delete the record?\n" + ownerCurr, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                context.Owners.Remove(ownerCurr);
                context.SaveChanges();
                RefreshLvOwners();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
