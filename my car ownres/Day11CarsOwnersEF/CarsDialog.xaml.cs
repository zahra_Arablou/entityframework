﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    public partial class CarsDialog : Window
    {
       // List<Car> carList = new List<Car>();
        CarsOwnersDbContext context;
        Owner currOwner;
        public CarsDialog(Owner owner)
        {
            
                InitializeComponent();
            try
            {
                context = new CarsOwnersDbContext();
            currOwner = owner;
            int id = currOwner.Id;
            lblOwnerId.Content = currOwner.Id;
            lblOwnerName.Content = currOwner.Name;
            refreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Fatal Error ", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1);
            }
        }
     //   int id;
        void refreshList()
        {
            try
            {
                // carList = (from c in context.Cars where c.Id==id select c).ToList<Car>();
                // List<Car> carList = currOwner.CarsInGarage.ToList<Car>();
                lvCars.ItemsSource = currOwner.CarsInGarage.ToList<Car>();
                Utils.AutoResizeColumns(lvCars);
                tbNameModel.Clear();
                btDelete.IsEnabled = false;
                btUpdate.IsEnabled = false;
                
              
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public bool IsDialogFieldsValid()
        {
            if (tbNameModel.Text.Length < 1)
            {
                MessageBox.Show("Please, fill in Make & Model", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!IsDialogFieldsValid()) { return; }
            try
            {
                string makeModel = tbNameModel.Text;

                Car car = new Car() { MakeModel = makeModel, OwnerId = currOwner.Id };
                context.Cars.Add(car);
                context.SaveChanges();
                // currOwner.CarsInGarage.Add(car);
                // Console.WriteLine(currOwner.CarsInGarage);
                 refreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            // DialogResult = true;
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            Car carCurr = (Car)lvCars.SelectedItem;
            if (carCurr == null) { return; }
            if (MessageBoxResult.No == MessageBox.Show("Do you want to delete the record?\n" + carCurr, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                Console.WriteLine(context.Entry<Car>(carCurr).State); // EntityState.Detached
                // context.Cars.Attach(carCurr);
                context.Cars.Remove(carCurr);
                context.SaveChanges();
                refreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!IsDialogFieldsValid()) { return; }
            Car carCurr = (Car)lvCars.SelectedItem;
            if (carCurr == null) { return; }
            try
            {
                carCurr.MakeModel = tbNameModel.Text;
                context.SaveChanges();
                refreshList();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }
        
        private void lvCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvCars.SelectedIndex == -1)
            {
                refreshList();
                return;
            }
            Car c = (Car)lvCars.SelectedItem;
            lblOwnerId.Content = c.OwnerId;
            tbNameModel.Text = c.MakeModel;
            btDelete.IsEnabled = true;
            btUpdate.IsEnabled = true;
        }
    }
}

