﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    class CarsOwnersDbContext:DbContext
    {
         public CarsOwnersDbContext() : base("CarOwnerDb") { }
        //const string DbName = "CarOwnerDb.mdf";
        //static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        //public CarsOwnersDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }
        virtual public DbSet<Car> Cars { get; set; }
        virtual public DbSet<Owner> Owners { get; set; }
    }
}
