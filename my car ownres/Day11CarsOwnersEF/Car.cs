﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
   
        public class Car
        {            
            public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string MakeModel { get; set; }
       
     //  [ForeignKey("Owner")]
        public int OwnerId { get; set; }
        
        //many to one relationship
        // single field - always eagerly loaded
        public virtual Owner Owner { get; set; }



        public override string ToString()
        {
            return $"{Id}, {MakeModel}";
        }
    }


   
}
