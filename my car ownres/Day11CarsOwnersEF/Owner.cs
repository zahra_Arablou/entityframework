﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
   public class Owner
    {[Key]
        public int Id {get; set;}
       
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [NotMapped]
        public int CarsNumber { get => CarsInGarage.Count(); } // compute only, don't store in DB

        [Required]
        public byte[] Photo { get; set; }

        public virtual ICollection<Car> CarsInGarage { get; set; } // one to many //ICollection<Car> CarsInGarage;

        public override string ToString()
        {
            return $"{Id}, {Name}";
        }
        // public Owner() : base() { }
    }
 
}
