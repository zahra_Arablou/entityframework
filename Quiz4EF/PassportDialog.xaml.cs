﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz4EF
{
    /// <summary>
    /// Interaction logic for PassportDialog.xaml
    /// </summary>
    public partial class PassportDialog : Window
    {
        byte[] personImage;
        Person currPerson;
        public PassportDialog(Person person)
        {
            InitializeComponent();
            try
            {
                currPerson = person;
                lblName.Content = person.Name;
                if (person.Passport == null)
                {
                    btnSave.Content = "Add";
                }
                else
                {
                    btnSave.Content = "Update";
                    tbPassNo.Text = person.Passport.PassportNo;
                    imageViewer.Source = Utils.GetBitmapImageV2(person.Passport.Photo);
                }
                Globals.ctx = new PersonDbContext();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1); // fatal error
            }


        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!IsFieldsValid()) { return; }
            try
            {
                Passport p = new Passport()
                {
                    PassportNo = tbPassNo.Text,
                    Photo = personImage,
                    Id = currPerson.Id,
                    //person=currPerson
                };
                //currPerson.passport = p;
                Globals.ctx.Passports.Add(p);
                Globals.ctx.SaveChanges();
                DialogResult = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public bool IsFieldsValid()
        {

            Regex regex = new Regex(@"^[A-Z]{2}[0-9]{6}$");
            if (!regex.IsMatch(tbPassNo.Text))
            {
                MessageBox.Show("PassPort format is 'AA123456'", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (personImage == null)
            {
                MessageBox.Show("Choose a picture", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    personImage = File.ReadAllBytes(dlg.FileName);
                    tbImage.Visibility = Visibility.Hidden;
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(personImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }
}
