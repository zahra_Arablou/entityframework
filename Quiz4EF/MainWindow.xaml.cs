﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz4EF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.ctx = new PersonDbContext();
                FetchRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                Environment.Exit(1); // fatal error
            }
        }
        public void FetchRecords()
        {
            try
            {
                lvPeople.ItemsSource = Globals.ctx.People.ToList();
                Utils.AutoResizeColumns(lvPeople);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void itemAddPerson_Click(object sender, RoutedEventArgs e)
        {
            PersonDialog personDialog = new PersonDialog() { Owner = this };
            personDialog.ShowDialog();
            FetchRecords();

        }

        private void lvPeople_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Person person = (Person)lvPeople.SelectedItem;
            PassportDialog passportDialog = new PassportDialog(person) { Owner = this };
            passportDialog.ShowDialog();
            FetchRecords();

        }
    }
}
