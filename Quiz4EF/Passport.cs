﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz4EF
{
   public class Passport
    {
        [ForeignKey("Person")]
        public int Id { get; set; }
       
        [Required]
        [StringLength(100)]
        public string PassportNo { get; set; }

       
         
         [Required]
        public byte[] Photo { get; set; }

      
        public virtual Person Person { get; set; }

    }
}
