﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz4EF
{
   public class PersonDbContext:DbContext
    {
        public PersonDbContext() : base("Quiz4Db") { }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Passport> Passports { get; set; }
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Passport>().Property(p => p.PassportNo).HasMaxLength(10);
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .HasOptional(p => p.Passport)
                .WithRequired(p => p.Person);
              base.OnModelCreating(modelBuilder);
        }
        //}
    }
}
